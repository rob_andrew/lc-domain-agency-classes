namespace lc.domain.agencyform
{
    public partial class AnnotatedDate
    {
        public System.DateTime DateTime { get; set; }
        public AnnotatedDateDateTimeType DateTimeType { get; set; }
    }
}