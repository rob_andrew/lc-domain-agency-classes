namespace lc.domain.agencyform
{
    public partial class ErrorMessage
    {
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }
        public string ErrorDetails { get; set; }
        public string StackTrace { get; set; }
    }
}