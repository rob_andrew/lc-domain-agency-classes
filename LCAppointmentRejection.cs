namespace lc.domain.agencyform
{
    public partial class LCAppointmentRejection
    {
        public AgencyFormHeader AgentHeader { get; set; }
        public string RejectionComments { get; set; }

        public LCAppointmentRejection()
        {
            this.AgentHeader = new AgencyFormHeader();
        }
    }
}