namespace lc.domain.agencyform
{
    public partial class Attachment
    {
        public string AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentURL { get; set; }
    }
}