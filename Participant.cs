namespace lc.domain.agencyform
{
    public class Participant
    {
        public string ParticipantCode { get; set; }
        public string ParticipantName { get; set; }
        public bool ExternalSource { get; set; }
    }
}