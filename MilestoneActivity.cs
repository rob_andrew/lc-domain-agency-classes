namespace lc.domain.agencyform
{
    public partial class MilestoneActivity
    {
        public int MilestoneSequence { get; set; }
        public int ActivityTypeId { get; set; }
        public System.DateTime MilestoneDate { get; set; }
        public string Notes { get; set; }
    }
}