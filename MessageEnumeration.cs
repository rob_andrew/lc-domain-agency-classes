namespace lc.domain.agencyform
{
    public enum MessageEnumeration
    {
        /// <remarks/>
        Appointment,

        /// <remarks/>
        AppointmentCancellation,

        /// <remarks/>
        AppointmentViewed,

        /// <remarks/>
        AppointmentRejection,

        /// <remarks/>
        AppointmentAcceptance,

        /// <remarks/>
        AgentFormStatusUpdate,

        /// <remarks/>
        ContactUpdate,

        /// <remarks/>
        PaymentNotification,

        /// <remarks/>
        PreArrivalForm,

        /// <remarks/>
        ArrivalForm,

        /// <remarks/>
        DailyForm,

        /// <remarks/>
        DepartureForm,

        /// <remarks/>
        StatementOfFactForm,

        /// <remarks/>
        DisbursementForm,

        /// <remarks/>
        AppointmentReceived,
    }
}