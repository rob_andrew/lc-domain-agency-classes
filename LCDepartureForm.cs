namespace lc.domain.agencyform
{
    using System.Collections.Generic;

    public partial class LCDepartureForm : LCAgencyFormBase<LCDepartureForm>
    {
        public System.DateTime CompletedLoadDischarge { get; set; }
        public System.DateTime CompletedLashing { get; set; }
        public System.DateTime Sailed { get; set; }
        public System.DateTime ETANextPort { get; set; }
        public StandardPort NextPort { get; set; }
        public System.DateTime ClearanceOutwards { get; set; }
        public System.DateTime DocsOBOutwards { get; set; }

        public List<CargoSummary> OriginalCargo { get; set; }

        public List<Stowplan> LoadedCargoes { get; set; }

        public List<Outturn> DischargedCargoes { get; set; }

        public BerthingCondition DepartureCondition { get; set; }

        public List<Berth> Berths { get; set; }

        public LCDepartureForm()
        {
            this.Berths = new List<Berth>();
            this.DepartureCondition = new BerthingCondition();
            this.DischargedCargoes = new List<Outturn>();
            this.LoadedCargoes = new List<Stowplan>();
            this.OriginalCargo = new List<CargoSummary>();
            this.NextPort = new StandardPort();
        }
    }
}