namespace lc.domain.agencyform
{
    public partial class CargoQuantity
    {
        public double Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}