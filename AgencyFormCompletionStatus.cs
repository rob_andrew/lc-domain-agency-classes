namespace lc.domain.agencyform
{
    public enum AgencyFormCompletionStatus
    {
        // ReSharper disable InconsistentNaming
        NEW = 0,
        DRAFT = 1,
        LOCAL_ERROR = 2,
        SUBMITTED = 3,
        QUEUED_TO_MASTER = 4,
        SENT_TO_MASTER = 5,
        MASTER_FAILED = 6,
        MASTER_RECEIVED = 7,
        OPERATOR_REJECTED = 8,
        OPERATOR_ACCEPTED = 9,
        OPERATOR_IGNORED = 10
        // ReSharper restore InconsistentNaming
    }
}