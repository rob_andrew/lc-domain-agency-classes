namespace lc.domain.agencyform
{
    public partial class AgencyContactContactDetails
    {
        public AgencyContactContactDetailsContactType ContactType { get; set; }
        public string ContactValue { get; set; }
    }
}