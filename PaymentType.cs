namespace lc.domain.agencyform
{
    public enum PaymentType
    {
        /// <remarks/>
        Actual,

        /// <remarks/>
        Advance,

        /// <remarks/>
        Draft,
    }
}