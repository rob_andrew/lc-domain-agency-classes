namespace lc.domain.agencyform
{
    public partial class Berth
    {
        public int BerthSequence { get; set; }
        public string BerthName { get; set; }
        public System.DateTime ArrivalTimestamp { get; set; }
        public BerthArrivalFlag ArrivalFlag { get; set; }
        public System.DateTime DepartureTimestamp { get; set; }
        public BerthDepartureFlag DepartureFlag { get; set; }
        public double CargoQuantity { get; set; }
        public string CargoQuantityUnitOfMeasure { get; set; }
    }
}