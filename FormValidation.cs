namespace lc.domain.agencyform
{
    public class FormValidation
    {
        public static FormValidation CreateOK;
        public string FormValidationCode { get; set; }

        public string FormValidationDescription { get; set; }

        public bool FormIsValid { get; set; }

        public bool FormInError { get; set; }

        public bool FormInWarning { get; set; }

        public string GetFormValidationMessage()
        {
            if (FormIsValid)
            {
                return "";
            }
            else
            {
                return string.Format("[{0}] {1}", FormValidationCode, FormValidationDescription);
            }
        }

        public static FormValidation CreateError(string errorCode, string additionalMessage)
        {
            var oFormValidation = new FormValidation
                                      {
                                          FormIsValid = false,
                                          FormValidationCode = errorCode,
                                          FormValidationDescription =
                                              GetErrorDescription(errorCode) + "<br />" + additionalMessage,
                                          FormInError = true,
                                          FormInWarning = false
                                      };

            return oFormValidation;
        }

        private static string GetErrorDescription(string errorCode)
        {
            throw new System.NotImplementedException();
        }

        public static FormValidation CreateWarning(string errorCode, string additionalMessage)
        {
            var oFormValidation = new FormValidation
                                      {
                                          FormIsValid = false,
                                          FormValidationCode = errorCode,
                                          FormValidationDescription =
                                              GetErrorDescription(errorCode) + "<br />" + additionalMessage,
                                          FormInError = false,
                                          FormInWarning = true
                                      };

            return oFormValidation;
        }

        public static FormValidation CreateOk()
        {
            var oFormValidation = new FormValidation
                                      {
                                          FormIsValid = true,
                                          FormValidationCode = "0",
                                          FormValidationDescription = "",
                                          FormInError = false,
                                          FormInWarning = false
                                      };

            return oFormValidation;
        }
    }
}