namespace lc.domain.agencyform
{
    public partial class CargoProduct
    {
        public int ProductGroupId { get; set; }
        public string ProductGroupName { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeName { get; set; }
    }
}