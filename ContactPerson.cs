namespace lc.domain.agencyform
{
    public partial class ContactPerson
    {
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactMobile { get; set; }
    }
}