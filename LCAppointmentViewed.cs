namespace lc.domain.agencyform
{
    public partial class LCAppointmentViewed
    {
        public AgencyFormHeader AgentHeader { get; set; }
        public string PersonViewedBy { get; set; }
        public System.DateTime TimeStamp { get; set; }

        public LCAppointmentViewed()
        {
            this.AgentHeader = new AgencyFormHeader();
        }
    }
}