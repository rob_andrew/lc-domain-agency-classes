using System;
using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public partial class LCPreArrivalForm : LCAgencyFormBase<LCPreArrivalForm>, IDisposable
    {
        public System.DateTime ArrivalOffPort { get; set; }
        public System.DateTime EstimatedTimeBerthing { get; set; }
        public System.DateTime ETD { get; set; }

        public void Dispose()
        {
            // do nothing.
        }
    }
}