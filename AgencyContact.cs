using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public partial class AgencyContact
    {
        public AgencyContactUserType UserType { get; set; }
        public string UserName { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ContactDetails")]
        public List<AgencyContactContactDetails> ContactDetails { get; set; }

        public AgencyContact()
        {
            this.ContactDetails = new List<AgencyContactContactDetails>();
        }
    }
}