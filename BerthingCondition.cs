namespace lc.domain.agencyform
{
    public partial class BerthingCondition
    {
        public double FuelOilQuantity { get; set; }
        public double DieselOilQuantity { get; set; }
        public double LSFIfo { get; set; }
        public double LNGQuantity { get; set; }

        public double FreshWater { get; set; }
        public double DraftsFore { get; set; }
        public double DraftsMid { get; set; }
        public double DraftsAft { get; set; }
        
        public BerthingConditionLngUnitOfMeasure LNGUnitOfMeasure { get; set; }
        public BerthingConditionFuelOilUnitOfMeasure FuelOilUnitOfMeasure { get; set; }
        public BerthingConditionDieselOilUnitOfMeasure DieselOilUnitOfMeasure { get; set; }
        public BerthingConditionLsfIfoUnitOfMeasure LSFIfoUnitOfMeasure { get; set; }
    }
}