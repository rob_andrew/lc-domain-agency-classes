namespace lc.domain.agencyform
{
    public enum AgencyContactContactDetailsContactType
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("E-MAIL")]
        Email,

        /// <remarks/>
        Mobile,

        /// <remarks/>
        Business,

        /// <remarks/>
        Fax,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("DIRECT PHONE")]
        DirectPhone,

        /// <remarks/>
        Home,

        /// <remarks/>
        Internet,

        /// <remarks/>
        Enterprise
    }
}