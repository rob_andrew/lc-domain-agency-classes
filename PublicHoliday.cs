using System;

namespace lc.domain.agencyform
{
    public partial class PublicHoliday
    {
        public int Sequence { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Notes { get; set; }
    }
}