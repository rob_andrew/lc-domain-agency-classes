using System;

namespace lc.domain.agencyform
{
    using System.Collections.Generic;

    public partial class LCStatementOfFactForm : LCAgencyFormBase<LCStatementOfFactForm>
    {
        public int ReportNumber { get; set; }
        public double TotalCargo { get; set; }
        public string TotalCargoUnitOfMeasure { get; set; }

        public DateTime ArrivalOffPort { get; set; }
        public DateTime? NORTendered { get; set; }
        public DateTime? WeatherTideDelayFrom { get; set; }
        public DateTime? WeatherTideDelayTo { get; set; }
        public DateTime? NORAccepted { get; set; }
        public DateTime? PratiqueGranted { get; set; }
        public DateTime AllFast { get; set; }
        public DateTime? CommencedLoadDischarge { get; set; }
        public DateTime? CommpletedLoadDischarge { get; set; }
        public DateTime ETD { get; set; }

        public List<LaytimeActivity> LaytimeActivities { get; set; }
        public List<PublicHoliday> PublicHolidays { get; set; }
        

        public LCStatementOfFactForm()
        {
            this.PublicHolidays = new List<PublicHoliday>();
            this.LaytimeActivities = new List<LaytimeActivity>();
        }
    }
}