using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public partial class ContactParticipant
    {
        public string ParticipantCode { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactAddress1 { get; set; }
        public string ContactAddress2 { get; set; }
        public string ContactSuburb { get; set; }
        public string ContactState { get; set; }
        public string ContactPostcode { get; set; }
        public string ContactCountry { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("ContactPerson")]
        public List<ContactPerson> ContactPerson { get; set; }

        public ContactParticipant()
        {
            this.ContactPerson = new List<ContactPerson>();
        }
    }
}