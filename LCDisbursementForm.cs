namespace lc.domain.agencyform
{
    using System.Collections.Generic;

    public partial class LCDisbursementForm : LCAgencyFormBase<LCDisbursementForm>
    {
        public System.DateTime DateIssued { get; set; }
        public string ReferenceNumber { get; set; }
        public string LocalCurrencyCode { get; set; }
        public string PaymentCurrencyCode { get; set; }
        public DisbursementFormType DisbursementFormType { get; set; }
        public string BankingDetailsForPayment { get; set; }

        public List<CostElementItem> CostElementItems { get; set; }

        public LCDisbursementForm()
        {
            this.CostElementItems = new List<CostElementItem>();
        }
    }
}