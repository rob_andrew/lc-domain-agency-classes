namespace lc.domain.agencyform
{
    public enum BerthArrivalFlag
    {
        /// Estimate
        E,

        /// Actual
        A,
    }
}