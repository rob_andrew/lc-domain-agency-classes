namespace lc.domain.agencyform
{
    public partial class Stowplan
    {
        public string Hold { get; set; }
        public CargoSummary Cargo { get; set; }
        public CargoProduct Product { get; set; }
        public CargoQuantity StowQuantity { get; set; }
        public StandardPort LoadPort { get; set; }
        public StandardPort DischargePort { get; set; }
        public System.DateTime StowDateTime { get; set; }
        public System.DateTime BillOfLadingDateTime { get; set; }

        public Stowplan()
        {
            this.DischargePort = new StandardPort();
            this.LoadPort = new StandardPort();
            this.StowQuantity = new CargoQuantity();
            this.Product = new CargoProduct();
            this.Cargo = new CargoSummary();
        }
    }
}