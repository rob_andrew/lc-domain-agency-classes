using System;
using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public partial class LCArrivalForm : LCAgencyFormBase<LCArrivalForm>
    {
        public LCArrivalForm()
        {
            Berths = new List<Berth>();
            ArrivalCondition = new BerthingCondition();
        }

        public DateTime ArrivalOffPort { get; set; }
        public DateTime? PratiqueGranted { get; set; }
        public DateTime? NORTendered { get; set; }
        public DateTime? WeatherTideDelayFrom { get; set; }
        public DateTime? WeatherTideDelayTo { get; set; }
        public DateTime? NORAccepted { get; set; }
        public DateTime? Anchored { get; set; }
        public DateTime? AnchorAweigh { get; set; }
        public DateTime? PilotOnBoard { get; set; }
        public DateTime? FirstLine { get; set; }
        public DateTime AllFast { get; set; }
        public DateTime? CommencedLoadDischarge { get; set; }
        public DateTime ETD { get; set; }
        public DateTime? ClearanceInwards { get; set; }
        public DateTime? DocsOBArrival { get; set; }

        public List<Berth> Berths { get; set; }
        public BerthingCondition ArrivalCondition { get; set; }
    }
}