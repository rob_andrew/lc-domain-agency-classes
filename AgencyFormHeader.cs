namespace lc.domain.agencyform
{
    public partial class AgencyFormHeader
    {
        // Message Id sent by an external system (or the Form Id if internal).
        public string AgentMessageId { get; set; }
        public System.DateTime MessageDate { get; set; }
        public MessageEnumeration MessageType { get; set; }
        public string MessageTypeVersion { get; set; }
        public SystemIdentifier SenderSystemId { get; set; }
        public SystemIdentifier RecieverSystemId { get; set; }
        // Source for external systems (origin system, eg VOSS)
        public string SourceMessageId { get; set; }
        
    }
}