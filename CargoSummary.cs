namespace lc.domain.agencyform
{
    public partial class CargoSummary
    {
        public int CargoId { get; set; }
        public CargoProduct Product { get; set; }
        public string ShipperParticipantCode { get; set; }
        public string CustomerParticipantCode { get; set; }
        public string Receiver { get; set; }
        public float EstimatedQuantity { get; set; }
        public float BillOfLadingQuantity { get; set; }

        public CargoSummary()
        {
            this.Product = new CargoProduct();
        }
    }
}