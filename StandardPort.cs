namespace lc.domain.agencyform
{
    public partial class StandardPort
    {
        public int PortId { get; set; }
        public string UNPortCode { get; set; }
        public string PortName { get; set; }
    }
}