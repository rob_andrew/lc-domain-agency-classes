namespace lc.domain.agencyform
{
    public partial class CargoOperation
    {
        public int CargoId { get; set; }
        public CargoProduct Product { get; set; }
        public ContactParticipant Shipper { get; set; }
        public ContactParticipant Customer { get; set; }
        public string Receiver { get; set; }
        public CargoQuantity BillofLadingQuantity { get; set; }
        public CargoQuantity EstimatedQuantity { get; set; }
        public CargoOperationLoadDischarge LoadDischarge { get; set; }

        public CargoOperation()
        {
            this.EstimatedQuantity = new CargoQuantity();
            this.BillofLadingQuantity = new CargoQuantity();
            this.Customer = new ContactParticipant();
            this.Shipper = new ContactParticipant();
            this.Product = new CargoProduct();
        }
    }
}