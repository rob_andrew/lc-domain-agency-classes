namespace lc.domain.agencyform
{
    public enum DisbursementFormType
    {
        /// <remarks/>
        Actual,

        /// <remarks/>
        Advance,

        /// <remarks/>
        Draft,
    }
}