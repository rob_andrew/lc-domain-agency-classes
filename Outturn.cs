namespace lc.domain.agencyform
{
    public partial class Outturn
    {
        public CargoSummary Cargo { get; set; }
        public CargoProduct Product { get; set; }
        public CargoQuantity OutturnQuantity { get; set; }
        public StandardPort LoadPort { get; set; }
        public StandardPort DischargePort { get; set; }
        public System.DateTime DischargeDateTime { get; set; }

        public Outturn()
        {
            this.DischargePort = new StandardPort();
            this.LoadPort = new StandardPort();
            this.OutturnQuantity = new CargoQuantity();
            this.Product = new CargoProduct();
            this.Cargo = new CargoSummary();
        }
    }
}