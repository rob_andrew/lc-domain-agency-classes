using System;

namespace lc.domain.agencyform
{
    public partial class LaytimeActivity
    {
        public int Sequence { get; set; }
        public int ActivityTypeId { get; set; }
        public System.DateTime ActivityFromDate { get; set; }
        public System.DateTime ActivityToDate { get; set; }
        public string Notes { get; set; }
        public DateTime? CreateTimestamp { get; set; }
        public DateTime? ModifyTimestamp { get; set; }
        public int Days { get; private set; }
        public int Hours { get; private set; }
        public int Minutes { get; private set; }

    }
}