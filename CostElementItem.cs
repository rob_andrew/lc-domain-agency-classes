using System;

namespace lc.domain.agencyform
{
    public partial class CostElementItem
    {
        // Unique Id
        public int CostElementItemId { get; set; }
        // Reference type
        public int CostElementId { get; set; }
        public string CostElementDescription { get; set; }
        public double LocalItemValue { get; set; }
        public double LocalItemGSTValue { get; set; }
        public double PaymentItemValue { get; set; }
        public double PaymentItemGSTValue { get; set; }
        public string Voucher { get; set; }
        public bool GSTApplicable { get; set; }
        public DateTime? CreateTimestamp { get; set; }
        public DateTime? ModifyTimestamp { get; set; }

    }
}