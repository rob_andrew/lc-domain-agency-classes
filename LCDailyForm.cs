using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public partial class LCDailyForm : LCAgencyFormBase<LCDailyForm>
    {
        public LCDailyForm()
        {
            this.Berths = new List<Berth>();
        }

        public int ReportNumber { get; set; }
        public System.DateTime CargoSituationFrom { get; set; }
        public System.DateTime CrgoSituationTo { get; set; }
        public System.DateTime ETD { get; set; }
        public double LoadedDischargedQuantity { get; set; }
        public double RemainingQuantity { get; set; }
        public string CargoUnitOfMeasure { get; set; }

        // Making the assumption we dont need to store calculated fields..
        public double AverageRate { get; private set; }
        public double PreviousTotal { get; private set; }
        public double TotalToDate { get; private set; }
        public double AverageRateToDate { get; private set; }


        public List<Berth> Berths { get; set; }

    }
}