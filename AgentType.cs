namespace lc.domain.agencyform
{
    public partial class AgentType
    {
        public string AgentTypeCode { get; set; }
        public string AgentTypeDescription { get; set; }
    }
}