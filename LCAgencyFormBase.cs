﻿using System;
using System.Collections.Generic;

namespace lc.domain.agencyform
{
    public class LCAgencyFormBase<T>
    {
        public int AppointmentId { get; set; }
        public int VersionNumber { get; set;  }
        public string CargoSummary { get; set; }
        public string FormId { get; set;  }
        public Participant AgentParticipant { get; set; }
        public bool ExternalSource { get; set; }

        public DateTime? CreateTimestamp { get; set; }
        public DateTime? ModifyTimestamp { get; set; }
        public DateTime? SubmitTimestamp { get; set; }

        public string CreateAuthenticationId { get; set; }
        public string ModifyAuthenticationId { get; set; }

        public bool IsDeleted { get; set; }

        public AgencyFormHeader AgentHeader { get; set; }

        public System.DateTime TimeStamp { get; set; }
        public string AgentName { get; set; }
        public AgencyFormCompletionStatus FormCompletionStatus { get; set; }
        public string Comments { get; set; }

        public List<Attachment> Attachments { get; set; }
        public List<AgencyContact> AgentContactDetails { get; set; }

        // Need to do this??
        public List<FormValidation> FormValidations { get; set; }

        public LCAgencyFormBase()
        {
            this.AgentHeader = new AgencyFormHeader();
            this.Attachments = new List<Attachment>();
            this.AgentContactDetails = new List<AgencyContact>();
            this.AgentParticipant = new Participant();
            this.FormValidations = new List<FormValidation>();
        }

    }
}